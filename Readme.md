[![](https://github.com/jpoehnelt/in-solidarity-bot/raw/main/static//badge-for-the-badge.png)](https://github.com/apps/in-solidarity)
# Linux Configuration
## Linux comes with its inbuilt C code compiler named `gcc`
- **To create a directory in [Ubuntu](https://ubuntu.com/download/desktop) you need to run this command in your terminal `mkdir directoryname`**
- **To create a file for C code, run `gedit filename.c` in the terminal**
- **After complition of code writing press `save` button on the top right corner**
- **After saving the code return to terminal and run `gcc filename.c -o filename.o` for compilation**
- **If you are using `math.h` library then you have to `gcc filename.c -o filename.o -lm` for compilation**
- **After successful compilation without errors, run `./filename.o` for running the code**

# Windows Configuration
## Windows never comes with any inbuilt C code compiler
- **Install an external ide. My suggestion is [this](https://sourceforge.net/projects/dev-cpp/files/Binaries/Dev-C%2B%2B%204.9.9.2/devcpp-4.9.9.2_setup.exe/download). Don't use Visual Studio Code**
- **In that external ide you can directly create folders and code files by clicking guided buttons**
- **In external ide you can directly and automatically compile and run your code**

# Technicalities
## Topics you should learn before seeing this codes
- **Data types in C**
- **Basic operations in C**
- **Functions in C**
- **Recursion method**
- **Loops and conditions**
- **Arrays in C**
- **Pointers in C**
- **Strings and structures**
- **Files in C**
- **Other minor topics**
## Size of various data types in C
| Data Type| Indicator| Specifier| Size  |
| -------- | -------- | ---------|-------|
| Integer  | int      | %d       | 2 byte|
| Decimal  | float    | %f       | 4 byte|
| Character| char     | %c       | 1 byte|